const config = require('./config');
const rabbitmq = require('./rabbitmq')
const Log = require('./utils').log;
const util = require('util');
const puppeteer = config.puppeteer;
const moduleName = 'Legend Crawler';

function sleep(ms) {
    return new Promise((resolve) => {
      setTimeout(resolve, ms);
    });
} 

async function login(page) {
    try {
        Log(moduleName, util.format('Logando em: %s', config.legendas.url));
        const userName = config.legendas.user;
        const userPassword = config.legendas.password;
        await page.setUserAgent('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36')
        await page.goto(config.legendas.url, { waitUntil: 'networkidle2' });
        await page.$eval('input[name*=username]', (user, value) => user.value = value, userName);
        await page.$eval('input[name*=password]', (pass, value) => pass.value = value, userPassword);
        const form = await page.$('form[class=login_box]');
        await form.evaluate(form => form.submit());
        await page.waitForNavigation();``
        const user = await page.$eval('section > div.login > a', sendBy => sendBy.innerText.trim());
        if (user !== userName)
            throw util.format("Não foi possível logar o usuário '%s'", userName);
        Log(moduleName, util.format('Logado como: %s', user));
        return page;
    } catch (err) {
        Log(moduleName, util.format("Erro no 'login': %s", err));
        await sleep(10000);
        return await login(page);
    }
}

async function searchLegends(page, search, key) {
    try {
        Log(moduleName, util.format("Pesquisando de legendas para '%s'", search));
        var legendPageCount = 0;
        var legendTotalCount = 0;
        var pageNum = 0;
        do {
            await page.goto(encodeURI(`${config.legendas.url}/legenda/busca/${search}/1/-/${pageNum}/-`), { waitUntil: 'networkidle2' });
            const urls = await page.evaluate(() => {
                const links = Array.from(document.querySelectorAll('article > div > div > p:first-child > a'));
                return links.map(anchor => anchor.href);
            });

            for (let url of urls) {
                rabbitmq.sendToQueue(config, JSON.stringify({ url: url, key: key }))
            }

            legendPageCount = urls.length;
            legendTotalCount += legendPageCount;
            pageNum += 1
        } while (legendPageCount > 0)
        rabbitmq.sendToQueue(config, JSON.stringify({ length: legendTotalCount }), key);
        Log(moduleName, util.format("Total de legendas encontradas: %s", legendTotalCount));
    } catch (err) {
        Log(moduleName, util.format("Erro no 'searchLegends': %s", err));
        await sleep(10000);
        await searchLegends(page, search, key);
    }
}

async function getLegend(page, url, key) {
    try {
        await page.goto(url, { waitUntil: 'networkidle2' });
        const legend = {
            Nome: await page.$eval('section > h1', name => name.innerText.trim()),
            Downloads: parseInt(await page.$eval('section > aside > p > span.number', downloads => downloads.innerText.trim())),
            EnviadoPor: await page.$eval('section > aside > p > span.nume', sendBy => sendBy.innerText.trim()),
            EnviadoEm: await page.$eval('section > aside > p > span.date', sendWhen => sendWhen.innerText.trim()) + ' ' + await page.$eval('section > aside > p > span.hour', sendWhen => sendWhen.innerText.trim())
        }
        const language = await page.$$eval('section > h1 > img', options => options.map(option => option.getAttribute('title').trim()));
        const like = parseFloat(await page.$eval('section > aside > p > a[href*=up]', like => like.parentElement.innerText.trim())) || 0;
        const dislike = parseFloat(await page.$eval('section > aside > p > a[href*=negativar]', dislike => dislike.parentElement.innerText.trim())) || 0;
        const link = await page.$$eval('section > button.icon_arrow', options => options.map(option => option.getAttribute('onclick').trim()));
        // const link = await page.$eval('section > button.icon_arrow', name => name.getAttribute('onclick').trim());
        if (link && link.length === 0) {
            Log(moduleName, "Usuário não logado!");
            await login(page);
            await getLegend(page, url, key);
        } else {
            legend['Idioma'] = language[0];
            legend['LikeRatio'] = parseFloat((like / (like + dislike) || 0.0).toFixed(2));
            legend['Nota'] = parseInt(legend['LikeRatio'] * 10);
            legend['Link'] = (link && link.length > 0) ? url.split('/').slice(0,3).join('/').concat(link[0].split("'")[1]) : undefined;
            rabbitmq.sendToQueue(config, JSON.stringify(legend), key);
            Log(moduleName, util.format("Legenda encontrada: %s", legend.Nome));
        }
    } catch (err) {
        Log(moduleName, util.format("Erro no 'getLegend': %s", err));
        await sleep(10000);
        await getLegend(page, url, key);
    }
}

function processMessage(page) {
    return async function(msg) {
        const message = JSON.parse(msg.toString());
        var promise;
        if (message && message.search) {
            await searchLegends(page, message.search, message.key);
        } else if (message) {
            await getLegend(page, message.url, message.key);
        }
    }
}

var browser;
const promise = puppeteer.launch({ args: ['--no-sandbox', '--disable-setuid-sandbox'] });
promise
    .then(browser => browser.newPage())
    .then(page => login(page))
    .then(page => rabbitmq.consumeQueue(config, processMessage(page)))
    .catch(err => Log(moduleName, util.format("Erro: %s", err)));
 