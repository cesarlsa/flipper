const amqp = require('amqplib/callback_api');
const util = require('util');
const puppeteer = require('puppeteer');

module.exports = {
    rabbitmq: {
        queueName: ReadConfig("AMQP_QUEUE_IN", "crawler", "amqpInOptions.queueName"),
        hostname: ReadConfig("AMQP_HOSTNAME", "192.168.0.42", "amqpInOptions.hostname"),
        port: ReadConfig("AMQP_PORT", "5672", "amqpInOptions.port"),
        vhost: ReadConfig("AMQP_VHOST", "", "amqpInOptions.vhost"),
        user: ReadConfig("AMQP_USER", "guest", "amqpInOptions.user"),
        pass: ReadConfig("AMQP_PASS", "guest", "amqpInOptions.pass"),
        prefetch: parseInt(ReadConfig("AMQP_PREFETCH", 1, "amqpInOptions.prefetch"))
    },
    legendas: {
        url: ReadConfig("LEGENDAS_URL", "http://legendas.tv", "legendas.url"),
        user: ReadConfig("LEGENDAS_USER", "fliperapp", "legendas.user"),
        password: ReadConfig("LEGENDAS_PASSWORD", "123456", "legendas.password")
    },
    amqp: amqp,
    util: util,
    puppeteer: puppeteer
};

function ReadConfig(environmentVariable, defaultValue, settingName) {
    var source = "EnvironmentVariable";
    var setting = process.env[environmentVariable];
    if (!setting) {
        setting = defaultValue;
        source = "DefaultValue";
    }
    Log(settingName + "=" + setting + " (" + source + ")");
    return setting;
}

function Log(msg) {
    console.log(new Date().toLocaleTimeString() + ' [Config] ' + msg);
}