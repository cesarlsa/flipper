const puppeteer = require('puppeteer');
(async () => {
    console.time('Teste');
    const browser = await puppeteer.launch();
    const page = await browser.newPage();

    await page.goto('http://legendas.tv/');

    await page.$eval('input[name*=username]', user => user.value = 'fliperapp');
    await page.$eval('input[name*=password]', pass => pass.value = '123456');
    const form = await page.$('form[class=login_box]');
    await form.evaluate(form => form.submit());
    await page.waitForNavigation();

    var legends = [];
    var legendPageCount = 0;
    var legendTotalCount = 0;
    var pageNum = 0;
    do {
        console.time(`Page${pageNum}`);
        await page.goto(`http://legendas.tv/legenda/busca/the%20simpsons/1/-/${pageNum}/-`);
        const urls = await page.evaluate(() => {
            const links = Array.from(document.querySelectorAll('article > div > div > p:first-child > a'));
            return links.map(anchor => anchor.href);
        });

        for (let url of urls) {
            const page2 = await browser.newPage();
            await page2.goto(url);
            const legend = {
                Nome: await page2.$eval('section > h1', name => name.innerText.trim()),
                Downloads: parseInt(await page2.$eval('section > aside > p > span.number', downloads => downloads.innerText.trim())),
                EnviadoPor: await page2.$eval('section > aside > p > span.nume', sendBy => sendBy.innerText.trim()),
                EnviadoEm: await page2.$eval('section > aside > p > span.date', sendWhen => sendWhen.innerText.trim()) + ' ' + await page2.$eval('section > aside > p > span.hour', sendWhen => sendWhen.innerText.trim())
            }
            const language = await page2.$$eval('section > h1 > img', options => options.map(option => option.getAttribute('title').trim()));
            const like = parseFloat(await page2.$eval('section > aside > p > a[href*=up]', like => like.parentElement.innerText.trim())) || 0;
            const dislike = parseFloat(await page2.$eval('section > aside > p > a[href*=negativar]', dislike => dislike.parentElement.innerText.trim())) || 0;
            const link = await page2.$eval('section > button.icon_arrow', name => name.getAttribute('onclick').trim());
            legend['Idioma'] = language[0];
            legend['LikeRatio'] = parseFloat((like / (like + dislike) || 0.0).toFixed(2));
            legend['Nota'] = parseInt(legend['LikeRatio'] * 10);
            legend['Link'] = url.split('/').slice(0,3).join('/').concat(link.split("'")[1]);
            console.log(legend);
            legends.push(legend);
            await page2.close();
        }
        legendPageCount = urls.length;
        legendTotalCount += legendPageCount;
        console.timeEnd(`Page${pageNum}`);
        pageNum += 1
    } while (legendPageCount > 0)
    console.log(legendTotalCount);

    await browser.close();
    console.timeEnd('Teste')
})();